#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# Source http://www.pythonchallenge.com/pc/return/5808.html
# username: huge
# password: file

s = ''

image_file="cave.jpg"

from PIL import Image, ImageDraw

im1 = Image.open(image_file)
draw1 = ImageDraw.Draw(im1)
im2 = im1.copy()
draw2 = ImageDraw.Draw(im2)
for w in xrange(im2.size[0]):
  for h in xrange(im2.size[1]):
    if im2.getpixel((w,h))[2]:
      draw2.point((w,h),(0,0,0,0))

im2.show()

print "You should new see a reddish scene with yellow 'evil' in upper right-hand corner"
print "Next page is evil.html"

s="evil.html"
# Pixels are odd/even interlaced pictures
# Odd row odd pixels are im1 and even pixels are im2
# Even row odd pixels are im2 and even pixels are im1

# Last thing to do
print "\nNext page: http://www.pythonchallenge.com/pc/return/%s\n" % s