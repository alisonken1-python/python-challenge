#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# Source http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=12345

import urllib

nothing = '12345'
baseURL = "http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing="

print
def getNothing(s='12345'):
  """"Retrieves the next linkedlist value

  The next pages in the chain are plain-text pages with the following format:
  
    "and the next nothing is xxxxx"

  until more instructions show up
  """
  #print "Retrieving %s" % s
  pg = urllib.urlopen("%s%s" % (baseURL, s))
  return pg.read()

for i in xrange(400):
  thisPage = getNothing(nothing)
  n = thisPage.split()[-1]

  if n.isdigit():
    nothing = n
    continue

  elif thisPage.find("Divide by two") > 0:
    nothing = int(nothing) / 2
    print "%s - using %s" % (thisPage, nothing)
    continue
  
  elif n.endswith(".html"):
    print thisPage
    s = n
    break

  else:
    print thisPage
    break

print "\nNext page: http://www.pythonchallenge.com/pc/dev/%s\n" % s
