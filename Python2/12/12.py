#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# Source http://www.pythonchallenge.com/pc/return/evil.html
# username: huge
# password: file

from PIL import Image, ImageDraw

image_file="evil2.gfx"
import os
for i in xrange(1,5):
  try:
    fp=open("evil2-%s.gfx" % i)
    fp.close()
    os.remove("evil2-%s.gfx" % i)
  except IOError as e:
    pass

s = ''
#im = Image.open(image_file)
f=open(image_file)
flen=67575L
d={ 1:open("evil2-1.gfx", "a"),
    2:open("evil2-2.gfx", "a"),
    3:open("evil2-3.gfx", "a"),
    4:open("evil2-4.gfx", "a"),
    5:open("evil2-5.gfx", "a") }

while f.tell() < flen:
  d[1].write(f.read(1))
  d[2].write(f.read(1))
  d[3].write(f.read(1))
  d[4].write(f.read(1))
  d[5].write(f.read(1))

d[1].close()
d[2].close()
d[3].close()
d[4].close()
d[5].close()

print "You should have 5 images that say:"
print "1 -  'dis'"
print "2 -  'pro'"
print "3 -  'port'"
print "4 -  'ional"
print "5 -  'ity' (with a line through this word)"

# Last thing to do
s="disproportional.html"
print "\nNext page: http://www.pythonchallenge.com/pc/return/%s\n" % s