#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-

"""
Challenge: http://pythonchallenge.com/pc/return/uzi.html

Page title: whom?

Picture: Calendar showing January 1**6 with Monday, 26 circled

Hint: Page source has "<!-- he ain't the youngest, he is the second -->"
Hint: Page source has "<!-- todo: buy flowers for tomorrow -->"
Hint: Subcalendar for Feb show 29 days - a leap year

Answer: Leap years between 1582 and the last day of the 20th Century
        are 1976 and 1756.
        Youngest date would be 1976.
        First hint indicates it's not the youngest date, but the second
        youngest date - 1756
        Monday, Jan 26, 1756 is Wolfgang Amadeus Mozart's birthday

Python learning: datetime module

Next challenge:
    http://www.pythonchallenge.com/pc/return/mozart.html

Solutions:
    http://www.pythonchallenge.com/pcc/return/mozart.html

"""

import datetime
MONDAY = 0
JANUARY = 1
FEBRUARY = 2
LEAPDAY = 29
DAY = 26
YEAR_FIRST = 1582  # First year of the Gregorian calendar
YEAR_LAST = 1996
found = False

for year in range(YEAR_FIRST, YEAR_LAST):
    if year % 10 != 6: continue
    try:
        check = datetime.date(year, FEBRUARY, LEAPDAY)
        leap = True
    except ValueError:
        leap = False
    if not leap: continue
    date = datetime.date(year, JANUARY, DAY)
    if date.weekday() == MONDAY:
        print("Monday January {}, {}".format(DAY, date.year))
