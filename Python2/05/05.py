#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# Source http://www.pythonchallenge.com/pc/def/peak.html

s = ""

# Pickle module
# Retrieve the pickled file from
# http://www.pythonchallenge.com/pc/def/banner.p

pickle_file="banner.p"
f = open(pickle_file)

import pickle
data = pickle.load(f)
f.close()

# data now contains a list of lists
# Each list equals (str, int) where:
#     str = what to print
#     int = how many times to print it

for i in data:
  l = ""
  for d in i:
    l = l + d[0]*d[1]
  print l

# The word that's printed is "channel"
s = "channel.html"

# Last thing to do
print "\nNext page: http://www.pythonchallenge.com/pc/def/%s\n" % s