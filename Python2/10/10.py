#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# Source http://www.pythonchallenge.com/pc/def/
s = ''

def __debug(msg=None):
  #print msg
  pass
  
def Morris(seq=None):
  __debug("Morris(seq='%s')" % seq)
  """Return Morris sequence of s as string

  Morris sequence describes the previous entry using only numbers.
  Example:
    a:      1 - First entry
    b:     11 - Describe a as "One 1"
    c:     21 - Describe b as "Two 1's"
    d:   1211 - Describe c as "One 2 and One 1"
    e: 111221 - Describe d as "One 1, one 2, two 1"
    f: 312211 - Describe e as "Three 1, two 2, One 1"
    g: etc.
  """
  # Make sure we have something to work with
  if seq is None :
    __debug("Morris: seq=None")
    s = None
  elif type(seq) is not type(''):
    # Looking for a string
    __debug("Morris: type(seq) is not type('')")
    s = None
  elif len(seq) == 0:
    # Looking for at least one digit to play with
    __debug("Morris: len(seq) -- 0")
    s = None
  elif not seq.isdigit():
    # May be string, but needs to be decimal numbers
    __debug("Morris: not seq.isdigit()")
    s = None
  elif len(seq) == 1:
    s = '1%s' % seq
  else:
    # Now we have something to play with
    s = ''
    p = 0
    # Make a list of the different digits
    while p < len(seq):
      c = seq[p]
      x = p
      while x < len(seq) and seq[x] == c:
        x=x+1
      s = "%s%s%s" % (s, x-p, c)
      p=x
  return s

m = Morris('1')
ans=['1']
for c in xrange(0,32):
  __debug("Checking Morris sequence of '%s'" % m)
  ans.append(Morris(ans[c]))

s = "%s" % len(ans[30])

print "\nLength of a[30]=%s" % s
# Last thing to do
print "\nNext page: http://www.pythonchallenge.com/pc/return/%s.html\n" % s