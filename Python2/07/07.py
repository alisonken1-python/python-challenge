#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# Source http://www.pythonchallenge.com/pc/def/oxygen.html
s = ''

img_file = "oxygen.png"

from PIL import Image

pic = Image.open(img_file)
w,h = pic.size
img = pic.load()

# The grey bands are identified by R=B=G, so if the RGB values are the same,
# that's the data we want to use.

t = 0
s1 = ''
s2 = ''
v  = False
i1 = 0
for i in xrange(h):
  r,g,b,a = img[0,i]
  if r==g and g==b:
    print s1
    # Due to a coding error in the image, the first character is repeated 5
    # times, but the rest of the characters are repeated 7 times
    s1 = chr(img[0,i][0])
    for j in xrange(5,w,7):
      if img[j,i][0] == img[j,i][1] and img[j,i][1] == img[j,i][2]:
        t = chr(img[j,i][0])
        s1 = s1 + t
        if v or t=='[':
          # Start new conversion
          s2 = s2 + t
          v = True

        if t == ']':
          s2 = s2 + ","
          v = False
          break

print s1
s3 = eval(s2)
s2 = s3[0]
for i in s2:
  s = s + chr(i)
# Last thing to do
print "\nNext page: http://www.pythonchallenge.com/pc/dev/%s.html\n" % s