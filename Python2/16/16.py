#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-

"""
Challenge: http://www.pythonchallenge.com/pc/return/mozart.html

Page title: let me get this straight
Picture: image with static
Hint: pink pixels sprinkled throughout image

Answer: Shift pixels until the bars line up properly

New challenge: http://www.pythonchallenge.com/pc/return/romance.html

"""

from PIL import Image

old_name = 'mozart.gif'
temp_name = 'mozart-temp.gif'
new_name = 'mozart-new.gif'

old = Image.open(old_name)
old.load()
x_size, y_size = old.size

new = Image.new(old.mode, (x_size*2, y_size))
new.palette = old.palette

magenta = 195  # Color bar we are looking 
bars = list()

# Get list of (xy) location for magenta bars
print('Getting old image')
for y in range(y_size):
    for x in range(x_size):
        pixel = old.getpixel((x,y))
        if pixel == magenta and old.getpixel((x+4, y)) == magenta:
            bars.append((x,y))

# Shift image so bars line up
print('Creating shifted image')
for y in range(y_size):
    for x in range(x_size):
        pixel = old.getpixel((x,y))
        new.putpixel((x+x_size-bars[y][0]%x_size, y), pixel)

new.save(temp_name)

# Clean up the image
print('Cleaning up temp image')
old = Image.open(temp_name)
old.load()
new = Image.new(old.mode, (x_size, y_size))
for y in range(y_size-1):
    for x in range(x_size):
        pixel = old.getpixel((x,y))
        pixel = old.getpixel((x+x_size,y)) if pixel < 5 else pixel
        new.putpixel((x,y), pixel)
new.save(new_name)