#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# Source page http://www.pythonchallenge.com/pc/def/map.html

import string
sFrom='abcdefghijklmnopqrstuvwxyz'
sTo=  'cdefghijklmnopqrstuvwxyzab'
sData="""g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb
gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb.
lmu ynnjw ml rfc spj."""

print "\nData string:\n %s\n" % sData

print "Translated string:\n %s\n" % string.translate(sData, string.maketrans(sFrom, sTo))

print "Next page: http://www.pythonchallenge.com/pc/dev/%s.html\n" % \
  string.translate('map', string.maketrans(sFrom, sTo))
  