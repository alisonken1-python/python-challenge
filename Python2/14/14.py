#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-

# Source http://www.pythonchallenge.com/pc/return/italy.html
# username: huge
# password: file
'''
Start with (10,000, 1) bit png image (wire.png)
Covert to a 100x100 image by starting at (0,0) and go clockwise in a
decreasing spiral until you reach (50,50)
'''

from PIL import Image

s='cat' # Answer for next page

image_name_old = "wire.png"
image_name_new = "wire-new.png"
old = Image.open(image_name_old)
old.load()
# 1-pixel high image, so we only get the X part to manipulate
old_length = old.size[0]

new = Image.new('RGB', (100,100), 0)

x = 0  # Starting point - 1
y = 0  # Starting point - 1

# Keep track of current new image x,y
size = 100  # Keep track of the current length of image to pull
pixel = 0  # Keep track of current pixel in old image

def swap_pixel(nx,ny, pixel):
    if pixel >= old_length:
        return pixel
    print('Copying pixel {} to ({}, {})'.format(pixel, nx, ny))
    new.putpixel((nx, ny), old.getpixel((pixel, 0)))
    pixel += 1
    return pixel

def start_end(start, count, forward=True):
    return (start, start+count) if forward else (start, start-count)

while pixel <= old_length:
    # pixel contains the current X of the old image to retrieve

    # Top row left -> right
    if pixel >= old_length: break
    (start, end) = start_end(x, size)
    print('Starting at ({}, {}) going right to ({}, {})'.format(start, y, end-1, y))
    for x in range(start, end):
        pixel = swap_pixel(x, y, pixel)
    size = size - 1
    
    # Right column top -> bottom
    if pixel >= old_length: break
    y += 1
    (start, end) = start_end(y, size)
    print('Continuing at ({}, {}) going down to ({}, {})'.format(x, start, x, end-1))
    for y in range(start, end):
        pixel = swap_pixel(x, y, pixel)

    # Bottom row right -> left
    if pixel >= old_length: break
    x -= 1
    (start, end) = start_end(x, size, forward=False)
    print('Continuing at ({}, {}) going left to ({}, {})'.format(start, y, end+1, y))
    for x in range(start, end, -1):
        pixel = swap_pixel(x, y, pixel)
    size = size - 1

    # Left column bottom -> top
    if pixel >= old_length: break
    y -= 1
    start, end = start_end(y, size, forward=False)
    print('Continuing at ({}, {}) going up to ({}, {})'.format(x, start, x, end+1))
    for y in range(start, end, -1):
        pixel = swap_pixel(x, y, pixel)

    print('Finished at ({}, {})'.format(x, y))
    x += 1

new.save(image_name_new)

# Last thing to do
print("\nNext page: http://www.pythonchallenge.com/pc/return/{}.html\n".format(s))