#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# Source http://www.pythonchallenge.com/pc/def/channel.html

# Download zip file http://www.pythonchallenge.com/pc/def/channel.zip

s = ""
zip_file = "channel.zip"

"""
channel.zip contains text files that are named <int>.txt files plus
a readme.txt file. readme.txt file has hints that says:

  hint1: start from 90052
  hint2: answer is inside the zip

Starting with 90052.txt, follow the same procedure as the previous
linkedlist challenge using these zipped text files
"""

import zipfile as z
mz = z.ZipFile(zip_file)

# First pass through the text files
nothing = '90052'
d = ''
while True:
  l = mz.read("%s.txt" % nothing)
  n = l.split()[-1]
  if n.isdigit():
    nothing = n
  else:
    break

# The final text says:
#   Collect the comments.
d = ''
nothing = '90052'
while True:
  l = mz.read("%s.txt" % nothing)
  n = l.split()[-1]
  if n.isdigit():
    d = d + mz.getinfo("%s.txt" % nothing).comment
    nothing = n
  else:
    print "\n", l, "\n"
    break

# Print the collected comments from each file
print d

"""
The comments print a banner that says "HOCKEY"
Going to http://www.pythonchallenge.com/pc/def/hockey.html
returns a text page that says:
  it's in the air. look at the letters.

The letters that are used to spell "hockey" consist of:
  H = O
  O = X
  C = Y
  K = G
  E = E
  Y = N
"""

print "\n Use the letters of the printed banner"

# Last thing to do
s = "oxygen.html"
print "\nNext page: http://www.pythonchallenge.com/pc/dev/%s\n" % s
