My playground for http://www.pythonchallenge.com

Summary of levels are:

    00 - Example challenge to get you used to changing the URL to point to the next challenge
    01 - maketrans (simple cypher substitution)
    02 - string search (characters in a block of noise)
    03 - string search using regex
    04 - linked list using requests module (internet webpage sockets)
    05 - pickle - create banner from pickled data
    06 - zip module - create banner from linked list in a zip file(zipped file separate download)
    07 - Search image for pixels that have r==g==b and convert to characters
    08 - BZ2 to decompress strings
    09 - Create an image, then draw lines using 2 arrays
    10 - Morris sequence (recursion doesn't work on my machine, so long-form)
    11 - Separate embedded image from a composite image
    12 - Split a file into separate files - new files are images
    13 - XMLRPC remote calls
    14 - Image manipulation - take a 10_000x1 image and rearrange to a 100x100 image
    15 - Playing with dates and the calendar module
    16 - Image manipulation - Rearrange lines to match vertical alignment pixels
    17 - Internet fun again - cookies with a linked list and XMLRPC calls and phonebook
    18 - Using DiffLib to create new images
    19 - Email, Mime attachments, and Wave files
