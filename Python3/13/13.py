#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 13

URL: http://www.pythonchallenge.com/pc/return/disproportional.html
"""
# This url is for challenges 00-08
# baseURL = "http://www.pythonchallenge.com/pc/def"
# This URL is for challenges 09-??

import xmlrpc

from xmlrpc.client import ServerProxy

baseURL = "http://www.pythonchallenge.com/pc/return"
baseEXT = "html"
answer = "XXXX"
un = 'huge'
pw = 'file'

print('\nChallenge 13: Looks like remote XMLRPC calls are due\n')


call_url = 'http://www.pythonchallenge.com/pc/phonebook.php'

client = ServerProxy(call_url)
print("Getting the number for Bert")
number = client.phone('Bert')
print('The number is {}'.format(number))

answer = 'italy'

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
