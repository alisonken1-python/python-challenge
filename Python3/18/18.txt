# Challenges 00-08 use /pc/def/
# Challenge: http://www.pythonchallenge.com/pc/def/

# Challenges 09-?? use /pc/return/
# Username = 'huge'
# Password = 'file'
Challenge: http://www.pythonchallenge.com/pc/return/balloons.html

Python learning: Image manipulation

Page title: can you tell the difference?

Picture: Side-by-side image of a swan at a lake, the right image is darker than
            the left image.

Hint: <!-- it is more obvious that what you might think -->

Answer: The obvious answer is 'brightness'. When you change the url to brightness.html,
        you get the same picture, but now the clue is:
            <-- maybe consider deltas.gz -->
        Now, change url to deltas.gz, and you get a file to work with.

        When looking into the file with gzip, you see hex digits that appear to be
        in 2 groups - left group and right group.

        Since the name of the file is "delta's", time to see what the difference is
        between the two sets; difflib would be a choice to use here.

        Split the two columns into  two arrays, then run difflib on them
        (make sure that the data really is _only_ the data part, and make sure
         there is only one \n at the end of each element)

        Once you run difflib, the difference will come back as:
            + 20 0f e4 01 07 06 0e aa 0a bb d4 44 4a 7a f8 3a 71 fc
              2c 83 1d c8 09 94 22 31 64 62 2c 3c 78 31 16 06 5e 8a
            - 15 08 04 02 21 50 81 40 20 10 02 15 08 04 02 21 50 81
        the + indicates that data is in the left side only
        the - indicates that data is in the right side only
        no +/- indicates that both sides have the same data
        Ignore lines that start with ?

        With this in hand, any line that:
            starts with '+' goes into image1
            starts with '-' goes into image2
            starts with ' ' goes into image3

        Once you split the data into three images, the new images come back as:
            'butter'
            'fly'
            '../hex/bin.html'

Next challenge: http://www.pythonchallenge.com/pc/hex/bin.html
                username: 'butter'
                password: 'fly'
