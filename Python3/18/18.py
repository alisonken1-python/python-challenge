#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge XXXX

URL: http://www.pythonchallenge.com/pc/return/
"""
import gzip
import difflib

from PIL import Image

baseURL = "http://www.pythonchallenge.com"

# This path is for challenges 00-08 and CGI scripts
pathBASE = 'pc/def'
# This path is for challenges 09-?? and requires a username/password
pathPASS = 'pc/return'
pathAnswer = pathPASS

baseEXT = "html"
answer = "XXXX"
un = 'huge'
pw = 'file'

print("\nChallenge 18: GZip and DiffLib\n")

data_source = 'deltas.gz'

def get_data(source_file):
    d1, d2 = [], []
    gz = gzip.open(source_file)
    # First, put the data into separate arrays
    for line in gz:
        print("Reading '{line}'".format(line=line))
        # Since the difflib is _very_ literal, we split the data
        # in specific columns so that we compare actual bytes rather
        # than extraneous spaces
        d1.append(line[:53].decode() + '\n')
        d2.append(line[56:-1].decode() + '\n')
        print("d1:       '{data}'".format(data=d1[-1].strip()))
        print("d2:       '{data}'".format(data=d2[-1].strip()))
    return (d1, d2)

def compare_data(d1, d2):
    # Now, see what the difference is
    print('Comparing the data ...')
    compare = difflib.Differ().compare(d1, d2)
    return compare

def save_images(data):
    # With the differences marked as:
    #   + data in left only
    #   - data in right only
    #   (no +/-) data in both
    # place the data into separate PNG files
    print('Splitting the differences into separate images ...')
    img1_name = 'img1.png'
    img2_name = 'img2.png'
    img3_name = 'img3.png'
    img1 = open(img1_name, "wb")
    img2 = open(img2_name, "wb")
    img3 = open(img3_name, "wb")

    for line in data:
        print('Checking {spacer}{line}'.format(spacer='' if line[0] in '+-' else '  ',
                                               line=line.strip()))
        if line.startswith('?'):
            # If it starts with a ?, then the line is pointing to
            # the only change(s) that are different within the line
            # example:
            #   1 2 3 4 5 6
            #   1 2 2 4 5 6
            # ?     ^
            continue
        bs = bytes([int(o, 16) for o in line[2:].strip().split(" ") if o])
        if line.startswith("+"):
            img1.write(bs)
        elif line.startswith("-"):
            img2.write(bs)
        else:
            img3.write(bs)
    img1.close()
    img2.close()
    img3.close()
    print("Images are saved as {one}, {two}, and {three}".format(one=img1_name,
                                                                 two=img2_name,
                                                                 three=img3_name))
    return(img1_name, img2_name, img3_name)

d1, d2 = get_data(data_source)
check = compare_data(d1, d2)
img1_file, img2_file, img3_file = save_images(check)

Image.open(img1_file).show()
Image.open(img2_file).show()
Image.open(img3_file).show()

pathAnswer = "pc/hex"
answer = "bin"
# Finally, print the next challenge URL
print("\nNext challenge: {site}/{path}/{nextpage}.{ext}\n".format(site=baseURL,
                                                                  path=pathAnswer,
                                                                  nextpage=answer,
                                                                  ext=baseEXT))
print("username: '{un}'    password: '{pw}'".format(un='butter', pw='fly'))
