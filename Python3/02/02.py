#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 02

Given a string of noise, find the characters that are rare.

Noisy string is saved as 02_source.txt.

URL: http://www.pythonchallenge.com/pc/def/ocr.html
"""
import string

baseURL = "http://www.pythonchallenge.com/pc/def"
baseEXT = "html"
answer = "XXXX"

source_file = '02_source.txt'

print('\nChallenge 02: Finding rare characters in a string of noise\n')

print("Noise text is found in file 02_source.txt")

# Basic assumption is that we are looking for characters in noise,
# where noise is defined as non-alphanumeric characters
text_list = []
gems = string.ascii_letters + string.digits

# Brute force method
def brute_force(block):
    gem_list = []
    for line in open(block, 'r'):
        for char in line.strip():
            if char in gems:
                gem_list.append(char)
    return gem_list

text_list = brute_force(source_file)
print("Gems found: {answer}".format(answer=text_list))

# Now put the gems together so we can find the next page
answer = ''.join(text_list)

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
