#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge XXXX

URL: http://www.pythonchallenge.com/pc/hex/
"""
baseURL = "http://www.pythonchallenge.com"

# This path is for challenges 00-08 and CGI scripts
pathBASE = 'pc/def'
# This path is for challenges 09-?? and requires a username/password
pathPASS = 'pc/hex'
pathAnswer = pathPASS

baseEXT = "html"
answer = "XXXX"
un = 'butter'
pw = 'fly'

print('\nChallenge XX: Short description here\n')


# Finally, print the next challenge URL
print("\nNext challenge: {site}/{path}/{nextpage}.{ext}\n".format(site=baseURL,
                                                                  path=pathAnswer,
                                                                  nextpage=answer,
                                                                  ext=baseEXT))
print("                  username: '{un}'   password: '{pw}'".format(un=un, pw=pw))
