#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge XXXX

URL: http://www.pythonchallenge.com/pc/def/integrity.html
"""
import bz2

baseURL = "http://www.pythonchallenge.com/pc/def"
baseEXT = "html"
answer = "XXXX"

print('\nChallenge 08: Time to learn BZ2 module\n')

un=b'BZh91AY&SYA\xaf\x82\r\x00\x00\x01\x01\x80\x02\xc0\x02\x00 \x00!\x9ah3M\x07<]\xc9\x14\xe1BA\x06\xbe\x084'
pw=b'BZh91AY&SY\x94$|\x0e\x00\x00\x00\x81\x00\x03$ \x00!\x9ah3M\x13<]\xc9\x14\xe1BBP\x91\xf08'

print("Click on the insect")
print("Username is '{name}'".format(name=bz2.decompress(un).decode('utf-8')))
print("Password is '{password}'".format(password=bz2.decompress(pw).decode('utf-8')))
