#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 17

URL: http://www.pythonchallenge.com/pc/return/romance.html
"""

# Looks like the results are a bzipped string
import bz2

# We use requests.get and requests.cookies.RequestCookieJar()
# The specific cookie we are looking for is cookiejar['info']
import requests
from requests.cookies import RequestsCookieJar
from urllib.parse import unquote_to_bytes
from xmlrpc.client import ServerProxy

baseURL = "http://www.pythonchallenge.com"

# This path is for challenges 00-08 and CGI scripts
pathBASE = 'pc/def'
# This path is for challenges 09-?? and requires a username/password
pathPASS = 'pc/return'
pathAnswer = pathPASS

baseEXT = "html"
answer = "XXXX"
un = 'huge'
pw = 'file'
max_links = 400

print('\nChallenge 17: Cookies! LinkedLists! Phonebook! Oh, My!.\n')

# Method for requesting text/cookies
def requests_method(url, keys, cookiejar):
    """
    Find the next number using base_url and nothing=next string to search
    Returns a tuple with (text, cookiejar)
    """
    try:
        req = requests.get(url, params=keys, cookies=cookiejar)
        if 'and the next busynothing ' in req.text:
            return (req.text.split()[-1], req.cookies)
        else:
            return (req.text, req.cookies)
    except OSError as e:
        return ("Error in request: {err}".format(err=e), None)

def find_busynothing(func, url, startkeys, jar, fh):
    # To speed things up, this is what's returned
    nothings = ['B', 'Z', 'h', '9', '1', 'A', 'Y', '%26', 'S', 'Y', '%94', '%3A', '%E2',
                'I', '%00', '%00', '%21', '%19', '%80', 'P', '%81', '%11', '%00', '%AF',
                'g', '%9E', '%A0', '+', '%00', 'h', 'E', '%3D', 'M', '%B5', '%23', '%D0',
                '%D4', '%D1', '%E2', '%8D', '%06', '%A9', '%FA', '%26', 'S', '%D4', '%D3',
                '%21', '%A1', '%EA', 'i', '7', 'h', '%9B', '%9A', '%2B', '%BF', '%60',
                '%22', '%C5', 'W', 'X', '%E1', '%AD', 'L', '%80', '%E8', 'V', '%3C', '%C6',
                '%A8', '%DB', 'H', '%26', '3', '2', '%18', '%A8', 'x', '%01', '%08', '%21',
                '%8D', 'S', '%0B', '%C8', '%AF', '%96', 'K', 'O', '%CA', '2', '%B0', '%F1',
                '%BD', '%1D', 'u', '%A0', '%86', '%05', '%92', 's', '%B0', '%92', '%C4',
                'B', 'c', '%F1', 'w', '%24', 'S', '%85', '%09', '%09', 'C', '%AE', '%24', '%90']
    return nothings

    keys = startkeys
    count = 0
    nothings = []
    # Hmmmm - looks like there's only 188 bytes in this chain
    for next in range(1, max_links):
        count += 1
        (reply, cookiejar) = func(url, keys, jar)
        if reply.startswith('Error '):
            # Ooops - problem with the network?
            nothings = None
            break
        elif cookiejar is None:
            print('Cookie jar is empty')
            break
        elif reply.isdigit() or reply.startswith("that's"):
            # Since busynothings seem to be numbers, quick check if we get a number back
            # should be sufficient
            print('Found another nothing: {nothing:5}  cookie: "{cookie}"'.format(nothing=reply,
                                                                                  cookie=cookiejar['info']))
            nothings.append(cookiejar['info'])
            if reply.startswith("that's"):
                # No more cookies for you
                break
            keys['busynothing'] = reply
            continue

    print('Iterations: {count}'.format(count=next))
    # print('Results: ', ''.join(nothings))
    print(reply)
    # print(nothings)
    return nothings

# nothing is used in challenge 04 linked list.
# busynothing is used in this challenge linked list.
# Since challenge 04 started with nothing=12345, we will too.
busynothing = {'busynothing': '12345' }
# Remember, the cookie we're looking for is 'info'
jar = RequestsCookieJar()

pagePHP = 'linkedlist.php'
urlPHP = '{host}/{path}/{cgi}'.format(host=baseURL,
                                      path=pathBASE,
                                      cgi=pagePHP)

message = find_busynothing(func=requests_method,
                           url=urlPHP,
                           startkeys=busynothing,
                           jar=jar,
                           fh=None)

# print('\nbusynothings = ', message)
# Convert from internet-encoding to bytes and decompress
print(bz2.decompress(unquote_to_bytes(''.join(message).replace('+', '%20'))).decode('ascii'))

# At this point we receive the message:
#   is it the 26th already? call his father and inform him that
#   "the flowers are on their way". he'll understand.

# OK - Looks like we're back to the phone book calling Mozart's father
pagePHP = 'phonebook.php'
pathBASE = 'pc'
urlPHP = '{host}/{path}/{cgi}'.format(host=baseURL,
                                      path=pathBASE,
                                      cgi=pagePHP)
client = ServerProxy(urlPHP)
print("\nGetting the number for Leopold")
number = client.phone('Leopold')
print('The number is {}'.format(number))

# When we go to violin.html, we get:
#   no! i mean yes! but ../stuff/violin.php.
# So, next page is actually /pc/stuff/violin.php

pathBASE = 'pc/stuff'
pagePHP = 'violin'
baseEXT = 'php'

# At this point, we get a page that says "no! i mean yes! but ../stuff/violin.php."
# So, go to that page and see what we have.

print("\nGo to {site}/{path}/{nextpage}.{ext}\n".format(site=baseURL,
                                                        path=pathBASE,
                                                        nextpage=pagePHP,
                                                        ext=baseEXT))

# The page is a picture of Leopold with the title "it's me, what do you want?"

# Now that we have Leopold on the line, tell him that the flowers are on their way.
url = '{base}/{path}/{page}.{ext}'.format(base=baseURL,
                                          path=pathBASE,
                                          page=pagePHP,
                                          ext=baseEXT)
url += "?info=the+flowers+are+on+their+way"
cookiejar = RequestsCookieJar()
cookiejar.set('info', 'the flowers are on their way')
req = requests.get(url, cookies=cookiejar)

# And what we get back in the page text is:
print("\noh well, don't you dare to forget the balloons.")

answer = 'balloons'

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
