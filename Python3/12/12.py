#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 12

URL: http://www.pythonchallenge.com/pc/return/evil.html
"""
# This url is for challenges 00-08
# baseURL = "http://www.pythonchallenge.com/pc/def"
# This URL is for challenges 09-??
from PIL import Image
from os import stat, remove
from time import sleep

print('\nChallenge 12: Split a file into 5 separate files, then create images from them\n')

baseURL = "http://www.pythonchallenge.com/pc/return"
baseEXT = "html"
answer = "XXXX"
un = 'huge'
pw = 'file'

img_file_name = 'evil2'
img_file_ext = 'gfx'
source_file_name = '{name}.{ext}'.format(name=img_file_name, ext=img_file_ext)
# Keep track of how many files we want to split into
deal_count = 5

# Since we're dealing with a binary file, let's get some basic info.
# The one we're interested in is the evil_stat.st_size parameter
#   so we can loop through the file and not go past the end.
evil_stat = stat(source_file_name)

def remove_old_files(basename, extension, count=deal_count):
    for f in range(1, count+1):
        try:
            remove('{name}_{count}.{ext}'.format(name=basename, count=f, ext=extension))
        except FileNotFoundError:
            pass

def get_file_list(basename, extension, count=deal_count):
    # returns a list of file names to save to
    file_list = []
    for fp in range(1, count+1):
        file_list.append('{name}_{count}.{ext}'.format(name=basename, count=fp, ext=extension))
    return file_list

def open_file_list(file_list):
    # Returns a list of open file handles based on the input file_list
    new_files = []
    for item in file_list:
        new_files.append(open(item, 'wb', buffering=0))
    return new_files

def split_files(source_file, file_name, ext, byte_count=evil_stat.st_size):
    # source_file is the string name of the source file
    # file_list is a list object where each element is a string of the filename to save to
    # file_name is a string of the base filename that we will use

    # Clean out old files
    remove_old_files(file_name, ext)

    # Get a list of file names to use
    new_file_list = get_file_list(file_name, ext)

    # Get a list of file handles to write to
    new_files = open_file_list(new_file_list)

    # Sequence through the source and split into the files
    source = open(source_file, 'rb', buffering=0)

    while source.tell() < byte_count:
        for fh in new_files:
            fh.write(source.read(1))

    # Close the files so we don't corrupt them
    for fh in new_files:
        fh.flush()
        fh.close()

new_extension = 'img'
split_files(source_file_name, img_file_name, new_extension)

print("You should now have 5 image files\n\n",
      "{name}_1.{ext} shows 'dis'\n".format(name=img_file_name, ext=new_extension),
      "{name}_2.{ext} shows 'pro'\n".format(name=img_file_name, ext=new_extension),
      "{name}_3.{ext} shows 'port'\n".format(name=img_file_name, ext=new_extension),
      "{name}_4.{ext} shows 'ional'\n".format(name=img_file_name, ext=new_extension),
      "{name}_5.{ext} shows 'ity' with a line through it\n".format(name=img_file_name, ext=new_extension))

print("NOTE: On Fedora24, I had to use Gimp to open the images; otherwise the 3rd file would fail ",
      "with 'Cannot read Metadata'")

answer = 'disproportional'

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
