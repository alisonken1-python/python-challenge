#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 07

URL: http://www.pythonchallenge.com/pc/def/oxygen.html
"""
from PIL import Image

baseURL = "http://www.pythonchallenge.com/pc/def"
baseEXT = "html"
answer = "XXXX"

print('\nChallenge 07: Hmm - something about image processing\n')

# Image saved as oxygen.png
# Initialize image data
img_file = 'oxygen.png'
img = Image.open(img_file)

# Since some characters are not printable, add an offset to make them the correct value
# 32 is the first printable character in the ascii sequence
chr_check = lambda x: x if x>=32 else x+100

def is_gem(img, x, y):
    """
    PNG image point returns (r,g,b,a).
    Gem is defined where pixel at point X,Y has the values
        r == g == b

    img = image to check
    x = point X
    y = point Y
    Returns if r==g==b
    """
    r,g,b,a = img.getpixel((x,y))
    return r == g == b


def find_vertical(img, startX=0, startY=0, reverse=False):
    """
    Looking for the box area that defines the grey band.
    """
    # (top, bottom is y-axis)
    top = startY
    bottom = 0 if reverse else img.height
    # (left-to-right is x-axis)
    left = startX
    direction = -1 if reverse else 1
    result = []
    # Should be able to find the upper/lower bound within about 10 pixels on x-axis
    for x in range(0, 9):
        for y in range(top, bottom, direction):
            if is_gem(img, x, y):
                result.append(y)
                break

    if len(result) < 1:
        return None
    # To somewhat normalize the boundary, take the average of the samples
    return int(sum(result) / len(result))

box_top = find_vertical(img, 0, 0)
box_bottom = find_vertical(img, 0, img.height-1, True)

# Now that we have top and bottom boundaries, let's find the hidden message.
# For S&G, let's parse the approximate middle of the grey box only since
# (theoretically), all of the rows will be the same
y = box_top + ((box_bottom-box_top)/2)
# current keeps track so we only get one character from a sequence of the same character
# Since all r,g,b bytes are the same, we only need one

# Brute force method using for loop
def brute_force(img):
    result = []
    current = img.getpixel((0, y))[0]
    result.append(chr(chr_check(current)))
    for x in range(1, img.width):
        # Apparently, the picture is wider than the grey area
        # so check to make sure we don't go beyond the grey box
        if is_gem(img, x, y):
            check = img.getpixel((x, y))[0]
            if check != current:
                current = check
                # Now that we have the next part, convert it to a charater and add it to the list
                result.append(chr(chr_check(check)))

    # Now, convert the list of characters to a string
    return result

# Recursive method for getting characters
def recursive_function(img, x, y, build_list):
    # Y remains the same, so we only need to check along X-axis
    if not is_gem(img, x, y):
        return build_list

    current_x = x
    while img.getpixel((current_x, y)) == img.getpixel((current_x+1, y)):
        current_x += 1
    build_list.append(chr(chr_check(img.getpixel((x,y))[0])))
    return recursive_function(img, current_x+1, y, build_list)

result = ''.join(recursive_function(img, 0, y, []))
print(result)

# With the results, we get:
#   smart guy, you made it. the next level is [105, 10, 16, 101, 103, 14, 105, 16, 121]
#
# Now, to convert the digits to a list we can convert to a string
open_bracket = result.find('[')
close_bracket = result.find(']')
# Isolate the part that we want and convert to characters and make a string
answer = ''.join([ chr(chr_check(int(x))) for x in result[open_bracket + 1:close_bracket].split(',') ])

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
