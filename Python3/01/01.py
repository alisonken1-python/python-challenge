#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 01 - What about making trans?

With the following hint:
    K -> M
    O -> Q
    E -> G

the text is a basic cypher substitution where every letter is shifted two
places to the left (i.e., M was originally a K, Q was originally O, etc.).

To translate, shift the alphabet to the right (wrapped, so y=a, z=b)

URL: http://www.pythonchallenge.com/pc/def/map.html
"""
baseURL = "http://www.pythonchallenge.com/pc/def"
baseExt = "html"

print('\nChallenge 01: String manipulation using string.maketrans')

# Create an original string we can map.
# We double the length so we don't have to do a wrap function.
text_map = 'abcdefghijklmnopqrstuvwxyz' * 2

# Create a dictionary mapping as well
text_dict = {}
for letter in text_map[:27]:
    text_dict[letter] = text_map[text_map.index(letter) + 2]

# Using the join method so the string can be on one page when read.
text = ' '.join(["g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp.",
                 "bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle.",
                 "sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."])

print('\nOriginal text:')
print(text)

def bruteforce(map_text, map_string1, map_string2):
    """ Brute force method

    map_text is the string to convert
    map_string1 is the straight string
    map_string2 is map_string1 shifted by the appropriate index to revert the changes
    """
    l = []
    for letter in map_text:
        if letter in map_string:
            # If it's in the mapping string, convert it
            l.append(map_string2[map_string1.index(letter)])
        else:
            l.append(letter)
    return ''.join(l)

def maketrans_dict(map_text, map_dict):
    """Python 3 str.maketrans(dict) method

    Python2 maketrans is part of the string module
    Python3 maketrans is a string method

    map_text is the string to convert.
    map_dict is a dictionary where the key is the letter and value is the
        letter to translate the key to.
    """
    return map_text.translate(''.maketrans(map_dict))

def maketrans_string(map_text, map_string1, map_string2):
    """Python3 str.maketrans(original_string, map_string)

    Python2 maketrans is part of the string module
    Python3 maketrans is a string method

    map_text is the string to convert.
    map_string1 is the straight string.
    map_string2 is map_string1 shifted by the appropriate index to revert the changes.
    """
    return map_text.translate(''.maketrans(map_string1, map_string2))

# Brute force method:
# print(bruteforce(text, text_map, text_map[2:]))

# maketrans dictionary method
# print(maketrans_dict(text, text_dict))

# maketrans string method
# print(maketrans_string(text, text_map[:27], text_map[2:29]))

# Whichever method you prefer to use, the result is:
#  "i hope you didnt translate it by hand. thats what computers are for.
#   doing it in by hand is inefficient and thats why this text is so long.
#   using string.maketrans is recommended. now apply on the url."

print("\nConverted text:")
print(maketrans_dict(text, text_dict))

# Since we already have a dictionary method defined, we'll use that on the URL
# In this case, we'll only use the URL name (minus .html) and see what we get
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseExt,
                                                         nextpage=maketrans_dict('map', text_dict)))
