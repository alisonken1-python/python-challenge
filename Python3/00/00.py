#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-

"""
Python Challenge 00 - warming up

URL: http://www.pythonchallenge.com/pc/def/0.html
"""

nextpage = '274877906944.html'

# Last thing to do
print("\nNext challenge: http://www.pythonchallenge.com/pc/def/{nextpage}\n".format(nextpage=nextpage))
