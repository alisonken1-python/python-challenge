# Challenges 00-08 use /pc/def/
# Challenge: http://www.pythonchallenge.com/pc/def/

# Challenges 09-?? use /pc/return/
# Username = 'butter'
# Password = 'fly'
Challenge: http://www.pythonchallenge.com/pc/hex/

Python learning: Email and Mime attachments

Page title: please!

Picture: Map showing the country of India

Hint: email from leopold in the source

Answer: Time to learn email module.

    Save the email to a file - make sure you delete the HTML markup around it.
    Open the email and extract the attached audio data.

    Once you isolate the base64 uuencoded data, decode it and save the
    resulting audio to a file called 'indain.wav'

    This file will basically say "sorry"
    Going to sorry.html, you are asked 'what are you sorry about?'

    The next part, you have to decrease the sample rate by 2,
    and increase the framerate by 2.

    With the changes made, you now get 'sorry' superimposed over
    a ditty about 'you are an idiot, ha ha ha ha ha ha ha ha'

    So, the final answer is 'idiot.html'

Next challenge: http://www.pythonchallenge.com/pc/hex/idiot.html
