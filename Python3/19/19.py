#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 19

URL: http://www.pythonchallenge.com/pc/hex/bin.html

# NOTE: This works using Python 3.5
#       Some changes made in the email module for Python 3.6
"""
import email
import base64
import wave

baseURL = "http://www.pythonchallenge.com"

# This path is for challenges 00-08 and CGI scripts
pathBASE = 'pc/def'
# This path is for challenges 09-?? and requires a username/password
pathPASS = 'pc/hex'
pathAnswer = pathPASS

baseEXT = "html"
answer = "XXXX"
un = 'butter'
pw = 'fly'

print('\nChallenge 19: Email and Mime attachments\n')


source_file = 'leopold_email.txt'

print('Retrieving message')
# First, load the email into Message()
fp = open(source_file, 'r')
msg = email.message_from_file(fp)
fp.close()

print('Getting mime data')
# Now, we want to get the attached audio file
part = msg.get_payload()[0]

# The part we get is
# 'audio/x-wav; name="indian.wav"'
# Extract the name of the file we save to
a_name = part.get('Content-type').split(';')[1].split('=')[1].strip('"')

# Extract the encoded audio
data = part.get_payload()

# Now we can decode it
audio = base64.standard_b64decode(data.strip())

# Finally, save it to a file
f = open(a_name, 'wb')
f.write(audio)
f.close()

# Once you play the file, you hear 'sorry'
# When we change the url to 'sorry.html', we get plain text that says:
#   - "what are you apologizing for?"

print('Converting wave file')
# With a little more twiddling, we resample the audio
indian = wave.open('indian.wav', 'rb')
result = wave.open("result.wav", "wb")

# The original has channels=1, samplewidth=2, and framerate=11025
# Set the same number of channels
result.setnchannels(indian.getnchannels())

# However, let's only get 1/2 the samlpes
result.setsampwidth(indian.getsampwidth()//2)

# But, we double the frame rate
result.setframerate(indian.getframerate()*2)

# Now, save the new audio
frames = indian.readframes(indian.getnframes())
wave.big_endiana = 1
result.writeframes(frames)
result.close()

# Now, we get "sorry", but it's also mixed in with a little ditty:
# You are an idiot, ha, ha, ha, ha, ha, ha, ha
# (# of 'ha' may not be exact, but you get the picture)

# And changing the url to 'idiot.html', we get Leopold saying:
#   "Now you should apologize ..."
# with a link to the next level

answer = 'idiot'

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{path}/{nextpage}.{ext}\n".format(site=baseURL,
                                                                  path=pathAnswer,
                                                                  nextpage=answer,
                                                                  ext=baseEXT))
print("                  username: '{un}'   password: '{pw}'".format(un=un, pw=pw))
