#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 03

URL: http://www.pythonchallenge.com/pc/def/equality.html
"""
import re
from string import ascii_lowercase, ascii_uppercase

baseURL = "http://www.pythonchallenge.com/pc/def"
baseEXT = "html"
answer = "XXXX"

print('\nChallenge 03: Regular expressions\n')
print('Text saved in file 03_source.txt')

# Retrieve the noise file we want to search through
with open("03_source.txt", "r") as myfile:
    noise = ''.join([line.replace('\n', '') for line in myfile.readlines()])


# Brute force method
def brute_method(text):
    current = 0
    # Remember, no need to go past the end if there is no guard at the end
    last = len(text) - 9
    found = []
    def guard_check(txt, pos):
        # Hint says to look for small character with exactly 3 large
        # characters on each side.
        # Remember, a guard is EXACTLY 3 large characters on each side,
        # which means if there's 4 then it's not a guard.
        return (txt[pos] in ascii_lowercase) and \
                (txt[pos+1] in ascii_uppercase) and \
                    (txt[pos+2] in ascii_uppercase) and \
                        (txt[pos+3] in ascii_uppercase) and \
                            (txt[pos+4] in ascii_lowercase)

    while current < last:
        if guard_check(text, current):
            current += 4
            # Found a possible starting guard, check for ending guard
            if guard_check(text, current):
                # Found ending guard, so add letter to list
                found.append(text[current])
                current += 4
            else:
                continue
        else:
            # No match, so increment counter and try again
            current += 1
            continue

    return ''.join(found)

def re_method(text):
    # Hint says to look for small character with exactly 3 large
    # characters on each side.
    # Remember, a guard is EXACTLY 3 large characters on each side,
    # which means if there's 4 then it's not a guard.
    # guards = re.compile("[a-z][A-Z][A-Z][A-Z][a-z][A-Z][A-Z][A-Z][a-z]")
    # Shorthand version of above line
    guards = re.compile("[a-z][A-Z]{3}[a-z][A-Z]{3}[a-z]")

    # Now find all the guards, then return ONLY the characters inside the guard
    # as a string
    return ''.join([ item[4] for item in guards.findall(noise) ])


answer = re_method(noise)
# answer = brute_method(noise)

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
