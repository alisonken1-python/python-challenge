#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 05

URL: http://www.pythonchallenge.com/pc/def/peak.html
"""

import pickle
import requests
from pathlib import Path

baseURL = "http://www.pythonchallenge.com/pc/def"
baseEXT = "html"
answer = "XXXX"

print('\nChallenge 05: Peak Hell - sounds like pickle to me\n')

# The pickled information is found at baseURL+source_file
source_file = "banner.p"

def get_source(my_file):
    path = Path('./{file}'.format(file=my_file))
    if not path.exists():
        # Not saved locally, so get from the internet
        req = requests.get("{base}/{file}".format(base=baseURL, file=my_file))
        with open(my_file, "w") as outfile:
            outfile.write(req.text)
    with open(my_file, "rb") as p_file:
        data = pickle.load(p_file)
    return data

# Now that we have the data, it appears to be a list of list items.
# Each list item contains pairs of (C,N) data where C=character and N=number.
# The object is to take each list item as a line in a banner.
# Each list item equals one line in a banner.
# Take each C from each list item and repeat it N times for each list item on one line.
#
# Since the first and last lines are both (' ', 95), we can assume the banner is
# 95 characters wide.

# First, get each line item
for line_item in get_source(source_file):
    # Now, break down each line item into a set and build a line
    line = []
    for items in line_item:
        line.append(items[0] * items[1])
    # Last, print the line
    print(''.join(line))

# Banner printed is "channel"
answer = 'channel'

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
