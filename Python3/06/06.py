#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 06

URL: http://www.pythonchallenge.com/pc/def/channel.html
"""
import zipfile
import sys

baseURL = "http://www.pythonchallenge.com/pc/def"
baseEXT = "html"
answer = "XXXX"

source_file = 'channel.zip'

print('\nChallenge 06: Hmmm - zipper fun\n')

def read_next(zfile, tfile):
    """
    zfile is zip archive to read from
    tfile is file in archive to retrieve
    returns a tuple containing text from file and the comment attached to the file
    """
    # Extract the text from the file
    text = zfile.read(tfile).decode('utf-8')
    # Extract the comment for this file
    comment = zfile.getinfo(tfile).comment.decode('utf-8')
    # Cleanup the text, but leave the comments as-is
    return (text.strip(), comment)


# Zip file contains about 910 files. Except for readme.txt (84 bytes),
# each filename format is NNNN.txt where NNN starts with 29.txt and ends
# with 99905.txt. Each of the numbered files are 20 or 21 bytes long
zfile = zipfile.PyZipFile(source_file)

# See what readme.txt file says
text, comment = read_next(zfile, 'readme.txt')
print(text)

# readme.txt hint1: start from 90052
# readme.txt hint2: answer is inside the zip
#
# So, start unzipping at 90052.txt, read the instructions, and go from there.
# Think linkedlist but with files in a zip archive.

current = '90052'
# Each file is formatted exactly like the linked list
# with 'Next nothing is NNNNN' in the text
# Each comment is a character for the banner, so build a banner while we're at it
#
# After all of that, the last one is "Collect the comments"

banner = []
while current:
    # OK - possibility of an endless loop, but not sure what to look for yet
    check, comment = read_next(zfile, '{file}.txt'.format(file=current))
    if check.startswith('Next nothing is'):
        current = check.split()[-1]
        print('Found another nothing: {nothing:6} (comment: "{comment}")'.format(nothing=current,
                                                                                 comment=comment))
        banner.append(comment)
    else:
        current = None
        print(check, comment)

# Finally, print the banner
print(''.join(banner))

'''
Banner:

****************************************************************
****************************************************************
**                                                            **
**   OO    OO    XX      YYYY    GG    GG  EEEEEE NN      NN  **
**   OO    OO  XXXXXX   YYYYYY   GG   GG   EEEEEE  NN    NN   **
**   OO    OO XXX  XXX YYY   YY  GG GG     EE       NN  NN    **
**   OOOOOOOO XX    XX YY        GGG       EEEEE     NNNN     **
**   OOOOOOOO XX    XX YY        GGG       EEEEE      NN      **
**   OO    OO XXX  XXX YYY   YY  GG GG     EE         NN      **
**   OO    OO  XXXXXX   YYYYYY   GG   GG   EEEEEE     NN      **
**   OO    OO    XX      YYYY    GG    GG  EEEEEE     NN      **
**                                                            **
****************************************************************
 **************************************************************

'''

# After changing the URL to 'hockey.html', the return page says:
# "it's in the air. look at the letters"
#
# letters are 'oxygen'
answer = 'oxygen'

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
