#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 14

URL: http://www.pythonchallenge.com/pc/return/italy.html
"""
from PIL import Image

print('\nChallenge 14: Unwind the wire\n')

# This url is for challenges 00-08
# baseURL = "http://www.pythonchallenge.com/pc/def"
# This URL is for challenges 09-??
baseURL = "http://www.pythonchallenge.com/pc/return"
baseEXT = "html"
answer = "XXXX"
un = 'huge'
pw = 'file'

source_img = "wire.png"

# The wire image is a 10000x1 image, so needs to be "wound" into a proper
# 100x100 image.
img = Image.open(source_img)

img_new_size = (100, 100)
new_img = Image.new(img.mode, img_new_size)

def wind_image(old_img, new_img):
    # Function for the x-rows of image
    # old_x is the x-position of the old image to pull from
    # side True = X, False = Y
    # forward True is l-r for X, t-b for Y
    # forward False is r-l for X, b-t for Y
    # xy is Y if side, X if not side
    def add_row(old_img, new_img, start, end, old_x, xy, side=True):
        old_x_curr = old_x
        for point in range(start, end,
                          1 if (end > start) else -1):
            new_point = (point if side else xy, xy if side else point)
            old_pixel = old_img.getpixel((old_x_curr, 0))
            new_img.putpixel(new_point, old_pixel)
            old_x_curr += 1
        # Return is old_img new X position
        return old_x_curr

    # Since we already know the old image is 10_000x1, we can make some shortcut assumptions

    # Keep track of which pixel to pull from the old image
    img_x = 0

    # How many times around the new image we'll be going
    loops = (img_new_size[0]//2)

    # Now that we have the pixel list, let's try and build an image
    for ring in range(0, loops):
        # Add the top
        # start = X
        # xy = Y
        img_x = add_row(old_img=old_img, new_img=new_img,
                        start=ring, end=(100-ring), xy=ring,
                        old_x=img_x, side=True)

        # Add the right side
        # start = Y
        # xy = X
        img_x = add_row(old_img=old_img, new_img=new_img,
                        start=(ring+1), end=(100-ring), xy=(99-ring),
                        old_x=img_x, side=False)

        # Add the bottom
        # start = X
        # xy = Y
        img_x = add_row(old_img=old_img, new_img=new_img,
                        start=(98-ring), end=(ring-1), xy=(99-ring),
                        old_x=img_x, side=True)

        # Add the left side
        # start = Y
        # xy = X
        img_x = add_row(old_img=old_img, new_img=new_img,
                        start=(98-ring), end=ring, xy=ring,
                        old_x=img_x, side=False)

        # Make sure we don't go past the end of the old image
        if img_x >= img.width:
            break

wind_image(img, new_img)

# Since a 100x100 is kinda small on my machine, let's resize it and see what it looks like
scale = 4
new_img = new_img.resize((new_img.width*scale, new_img.height*scale))
new_img.show()

# new_img is the image of a cat.
answer = 'cat'

# When you look at the new challenge, the cat's name is 'uzi' with no other
# indication for the next challenge. So, try uzi.
answer = 'uzi'

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
