#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 11

URL: http://www.pythonchallenge.com/pc/return/5808.html
"""
# This url is for challenges 00-08
# baseURL = "http://www.pythonchallenge.com/pc/def"
# This URL is for challenges 09-??

from PIL import Image
from array import array

baseURL = "http://www.pythonchallenge.com/pc/return"
baseEXT = "html"
answer = "XXXX"
un = 'huge'
pw = 'file'

img_file = 'cave.jpg'

print('\nChallenge 11: Image processing - separate images embedded together\n')

# Brute force method.
# This method extracts the wanted bits and put's them into another image.
def extract_image(img):
    # returns an image using the parts where B=0 from RGB value.

    # Create the new image
    # Initial color value is a dark grey to enable the extracted image to be viewed easier.
    img1 = Image.new(mode=img.mode, size=(img.width, img.height), color=(26, 28, 30))

    # Define arrays for image data
    # Break out the image data into arrays
    for y in range(0, img.height):
        for x in range(0, img.width):
            r,g,b = img.getpixel((x,y))
            if b == 0:
                img1.putpixel((x,y), (r,g,b))

    return img1

# Another brute force method.
# This one just clears the pixels that ar not part of the image we want.
def blank_noise(img):
    # returns an image after resetting the noise pixels

    # Blank any pixel that is not part of the hidden message
    for y in range(0, img.height):
        for x in range(0, img.width):
            if img.getpixel((x,y))[2]:
                img.putpixel((x,y), (26, 28, 30))

    return img

img = Image.open(img_file)
# extract_image(img).show()
blank_noise(img).show()

# Image is faint, but you can make out "evil" in the upper-right area of the new image
answer = 'evil'

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
