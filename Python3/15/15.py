#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 15

URL: http://www.pythonchallenge.com/pc/return/uzi.html
"""
# This url is for challenges 00-08
# baseURL = "http://www.pythonchallenge.com/pc/def"
# This URL is for challenges 09-??


import datetime as dt

baseURL = "http://www.pythonchallenge.com/pc/return"
baseEXT = "html"
answer = "XXXX"
un = 'huge'
pw = 'file'

print('\nChallenge 15: Calendar functions\n')

# The date listed is Monday, January 26, 1**6
# The hint to remember is "<!-- todo: buy flowers for tomorrow -->"
# So, check for significant births on Tuesday, January 27 between 1006 and 1996
year_start = 1006
year_end = 1996
month = 1
day = 27
tuesday = 1

print('Checking for years where Tuesday, January 27 is on a Tuesday\n')

for year in range(year_start, year_end + 1, 10):
    check = (year, month, day)
    # print('Checking ', check, ': day of week: ', dt.date(year, month, day).weekday())
    if dt.date(year, month, day).weekday() == tuesday:
        print('Candidate year: ', year)

print("\n -> Tuesday, January 27, 1756 - Wolfgang Amadeus Mozart was born")
answer = 'mozart'

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
