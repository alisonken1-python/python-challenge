#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 04

URL: http://www.pythonchallenge.com/pc/def/linkedlist.php
"""
# Since we're doing simple HTTP stuff, use the new requests module
import requests
import socket

baseURL = "http://www.pythonchallenge.com/pc/def"
baseEXT = "html"
answer = "XXXX"

print('\nChallenge 04: Looks like a linked list search\n')

# Since we're going to go through a bunch of web searches, define some basics
current = "12345"
page = "linkedlist.php"
base_url = "{base}/{page}".format(base=baseURL, page=page)
max_links = 400

def requests_method(nothing):
    """
    Find the next number using base_url and nothing=next string to search
    """
    try:
        req = requests.get(base_url, params={'nothing': nothing})
        if 'and the next nothing ' in req.text:
            return req.text.split()[-1]
        else:
            return req.text
    except OSError as e:
        return "Error in request: {err}".format(err=e)

def find_nothing(func, current):
    for next in range(1, max_links):
        # For testing, it looks like it takes 251 iterations, and the answer is at
        # nothing = '66831'
        # current = '66831'
        reply = func(current)
        # Since nothings seem to be numbers, quick check if we get a number back
        # should be sufficient
        if '.html' in reply:
            # Found page
            answer = reply.split('.')[0]
        elif reply.isdigit():
            print('Found another nothing: {nothing}'.format(nothing=reply))
            current = reply
            continue
        elif 'Divide by two and keep going.' in reply:
            print('Found divide-by-two message')
            current = str(int(current) / 2)
            continue

        print('Iterations: {count}'.format(count=next))
        print(reply)
        break
    return reply

ans = find_nothing(requests_method, current)
if '.' in ans:
    answer = ans.split('.')[0]
else:
    print(ans)

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
