#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 10

URL: http://www.pythonchallenge.com/pc/return/bull.html
"""
# This url is for challenges 00-08
# baseURL = "http://www.pythonchallenge.com/pc/def"
# This URL is for challenges 09-??
baseURL = "http://www.pythonchallenge.com/pc/return"
baseEXT = "html"
answer = "XXXX"
un = 'huge'
pw = 'file'

print('\nChallenge 10: Morris sequence\n')

# WARNING: on my machine, a recursive function dies at around a[26] with recursions exceeded.
# Time to do it the hard way.

def Morris(check=None):
    # Since recursion is out, time to do it the hard way.
    # NOTE: check is a string, but must be digits
    # Validity checks
    if (check is None) or (len(check) < 1):
        # Gotta have something to work with
        return None
    elif type(check) is not type(''):
        # And it has to be a string
        return None
    elif not check.isnumeric():
        # Also, must be numbers and nothing else
        return None
    elif len(check) == 1:
        # Simple return
        return "1{digit}".format(digit=check)

    # OK - now something to play with

    start = 0
    current = 0
    build = []
    print('Morris check: {chk}'.format(chk=check))

    while current < len(check):
        digit = check[current]
        start = current
        while (current < len(check)) and (check[current] == digit):
            current += 1
        build.append('{count}{digit}'.format(count=current-start,
                                             digit=digit))
        start = current

    return ''.join(build)


# Start with what we know and go from there
a = ['1', '11', '21', '1211', '111221', ]
while len(a) <= 31:
    chk = Morris(a[-1])
    if chk is None: continue
    a.append(chk)

print('\nlen(a): {length}'.format(length=len(a)))
print('a[30]: ', a[30])
print('Length of a[30]: {length}'.format(length=len(a[30])))
answer = len(a[30])


# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
