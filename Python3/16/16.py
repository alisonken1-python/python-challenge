#!/usr/bin/env python3
# -*- coding: iso-8859-1 -*-
"""
Challenge 16

URL: http://www.pythonchallenge.com/pc/return/mozart.html
"""
# This url is for challenges 00-08
# baseURL = "http://www.pythonchallenge.com/pc/def"
# This URL is for challenges 09-??
from PIL import Image

baseURL = "http://www.pythonchallenge.com/pc/return"
baseEXT = "html"
answer = "XXXX"
un = 'huge'
pw = 'file'

print('\nChallenge 16: Image manipulation - line up the horizontal pixels\n')

img_source = 'mozart.gif'
mark_range = [ x for x in range(237, 253) ]
mark_pixel = 195

def lineup_copy(img):
    def shift_line(img1, img2, offset, y):
        # Offset is the number of pixels to shift left
        for x in range(0, img1.width):
            img2.putpixel((x, y), img1.getpixel(((offset+x) % img.width, y)))

    # Since we overwrite some areas we need during the shifting, use an image copy.
    img_2 = img.copy()
    for y in range(0, img.height):
        for x in range(0, img.width):
            # Look for the start/end pixels, and secondary check make sure
            # that the start+1 and end-1 pixels are the marker_pixel
            if (img.getpixel((x, y)) in mark_range) and \
               (img.getpixel((x+6, y)) in mark_range) and \
               (img.getpixel((x+1, y)) == mark_pixel) and \
               (img.getpixel((x+5, y)) == mark_pixel):
                   print('Found marker at ({x:3}, {y:3}) start: {s}  end: {e}'.format(x=x, y=y,
                                                                                      s=img.getpixel((x,y)),
                                                                                      e=img.getpixel((x+6,y))))
                   shift_line(img1=img, img2=img_2, offset=x, y=y)
                   break
    return img_2

img = lineup_copy(Image.open(img_source))
img.show()

answer = 'romance'

# Finally, print the next challenge URL
print("\nNext challenge: {site}/{nextpage}.{ext}\n".format(site=baseURL, ext=baseEXT,
                                                           nextpage=answer))
